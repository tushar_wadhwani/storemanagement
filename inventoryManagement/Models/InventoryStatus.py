from django.db import models
from model_utils.models import TimeStampedModel
from .Product import Product

class InventoryStatus(TimeStampedModel):
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    quantity_available = models.IntegerField(null=True)
    mrp_per_unit = models.DecimalField(null=True, max_digits=19, decimal_places=2)
    batch_date = models.DateField(null=True)
    batch_number = models.CharField(null=True, max_length=255)
