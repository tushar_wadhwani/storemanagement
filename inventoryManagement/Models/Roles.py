from django.db import models
from model_utils.models import TimeStampedModel

class Roles(TimeStampedModel):
    role = models.CharField(max_length=255, null=False)
