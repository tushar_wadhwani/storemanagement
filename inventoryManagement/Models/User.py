from django.contrib.auth.models import AbstractUser
from django.db import models
from model_utils.models import TimeStampedModel

from .Roles import Roles


class User(AbstractUser, TimeStampedModel):

    roles = models.ManyToManyField(Roles)

    class Meta:
        db_table = 'auth_user'
