from django.db import models
from model_utils.models import TimeStampedModel


class Product(TimeStampedModel):
    name = models.CharField(max_length=255, null=True)
    vendor = models.CharField(max_length=255, null=True)
