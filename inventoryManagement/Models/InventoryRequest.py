from django.db import models
from model_utils.models import TimeStampedModel
from .User import User
from .Product import Product


class InventoryRequest(TimeStampedModel):
    APPROVED = "APPROVED"
    PENDING = "PENDING"
    STATUS_CHOICE = (
        (APPROVED, APPROVED),
        (PENDING, PENDING)
    )
    requested_by = models.ForeignKey(User, related_name="inventory_requested", on_delete=models.SET_NULL, null=True)
    approved_by = models.ForeignKey(User, related_name="inventory_approved", on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    quantity = models.IntegerField(null=True)
    price_per_unit = models.DecimalField(null=True, max_digits=19, decimal_places=2)
    total_price  = models.DecimalField(null=True, max_digits=19, decimal_places=2) # total price of order including taxes
    batch_date = models.DateField(null=True)
    batch_number = models.CharField(null=True, max_length=255)
    status = models.CharField(choices=STATUS_CHOICE, null=False, max_length=255)
