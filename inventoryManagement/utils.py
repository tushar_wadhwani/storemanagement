import logging
import json
from django.http import HttpResponse

from rest_framework_jwt.settings import api_settings

logger = logging.getLogger(__name__)

response_message_code = {
    100: 'The request body is not valid JSON.'
}


def get_jwt_token(user):
    try:
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token
    except Exception as e:
        logger.critical("Exception in JWT token - {}".format(str(e)))
        return None

def get_success_response(data, status_code=200):
    # TODO: Handle loads and dumps error
    j_code = json.dumps(data)

    return HttpResponse(j_code, status=status_code, content_type='application/json')


def get_failure_response(message_code, status_code):
    # TODO: Handle Keyerror
    try:
        j_code = json.dumps({'error': response_message_code[message_code], 'status_code': status_code})
    except KeyError:
        j_code = json.dumps({'error': str(message_code), 'status_code': status_code})

    return HttpResponse(j_code, status=status_code, content_type='application/json')
