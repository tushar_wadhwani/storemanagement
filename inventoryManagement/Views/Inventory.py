import json

from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView

from django.contrib.auth import get_user_model
from django.http import JsonResponse

from ..utils import get_failure_response
from inventoryManagement.Models import Product, InventoryStatus, InventoryRequest
from ..Helpers import request_inventory

User = get_user_model()

class Inventory(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        pass

    def post(self, request):
        try:
            data = json.loads(request.body)
        except (TypeError, json.JSONDecodeError):
            return get_failure_response(100, status.HTTP_400_BAD_REQUEST)
        product_id = data.get("product_id")
        if not product_id:
            # Add new  product
            product = Product(
                                name=data["product_name"],

                                vendor=data["product_vendor"]
                              )
            product.save()
        else:
            # Product already exist
            product = Product.objects.get(pk=product_id)

        # Request Inventory for product
        inventory_request_data = request_inventory(request.user, product, data)
        return JsonResponse({"inventory_request": inventory_request_data}, status=status.HTTP_200_OK)


    def patch(self, request):

        pass