import json

from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from django.contrib.auth import get_user_model

from ..utils import get_jwt_token, get_failure_response, get_success_response
from ..Validators import LoginSerializer

User = get_user_model()

class Login(APIView):
    permission_classes = [AllowAny]

    def post(self, request):

        try:
            data = json.loads(request.body)
        except (TypeError, json.JSONDecodeError):
            return get_failure_response(100, status.HTTP_400_BAD_REQUEST)

        serializer = LoginSerializer(data=data)
        if not serializer.is_valid():
            return get_failure_response((list(serializer.errors.values()))[0][0], status.HTTP_400_BAD_REQUEST)

        user_email = data["email"]
        user_password = data["password"]

        try:
            user = User.objects.get(email=user_email)
        except User.DoesNotExist:
            return get_failure_response("The user with this email ID is not registered with us. Try another ID or "
                                        "sign up instead.", status.HTTP_404_NOT_FOUND)

        is_correct_password = user.check_password(user_password)

        if is_correct_password:
            jwt_token = get_jwt_token(user)
            token_response = {'token': jwt_token}
        else:
            return get_failure_response("Password is incorrect. Please try again or click on Forgot Password to reset.",
                                        status.HTTP_401_UNAUTHORIZED)

        return get_success_response(token_response)
