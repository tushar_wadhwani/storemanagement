from django.conf.urls import url
from rest_framework import routers
from django.urls import path

from .views import Login
router = routers.DefaultRouter()

urlpatterns = [
    path('login/', Login.as_view())
]
