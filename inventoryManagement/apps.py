from django.apps import AppConfig


class InventorymanagementConfig(AppConfig):
    name = 'inventoryManagement'
